/**
 * Created by Elaine Hong on 11/7/2016.
 */

var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());


app.get("/register", function(req , res){
    console.info(req);
   
    
    var email = req.body.params.email;
    var password = req.body.params.password;
    var name = req.body.params.name;
    var gender = req.body.params.gender;
    var dob = req.body.params.dob;
    var address = req.body.params.address;
    var country = req.body.params.country;
    var contact = req.body.params.contact;
    
    
    
    console.info("Email :%s", email);
    console.info("Password :%s", password );
    console.info("Name :%s", name);
    console.info("Gender :%s", gender);
    console.info("Date of Birth:%s", dob);
    console.info("Address :%s", address);
    console.info("Country :%s", country);
    console.info("Contact :%s", contact);
    
        
    res.status(200).end();
});

app.get("/thankyou", function(req , res) {
    res.redirect("thankyou.html");
});



app.use(express.static(__dirname +"/public"));


app.set("port",process.argv[2]|| process.env.APP_PORT||3000);
app.listen(app.get("port"),function(){
    console.info("Application started on port %d", app.get("port"));
});

